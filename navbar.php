<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from azim.commonsupport.com/Acto/index-8.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Apr 2020 10:10:44 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    
    <meta property="og:type" content="website"/>
	<meta property="og:title" content="Berium Healthcare : Supplier of Medical Equipment, Devices, Instruments and consumables 
"/>
	<meta property="og:image" content="images/newlogo2.png"/>
	<meta property="og:description"
	      content="We have a vision for a healthy Africa. We work with the best medical equipment manufacturers in the world to help us solve local challenges. Talk to us for high quality medical equipment and consumables. We care."/>
    

    <title>Berium Medical Supply</title>

    <!-- Stylesheets -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
<!--    <link rel="icon" href="images/favicon.ico" type="image/x-icon">-->
    <script src="alert/alertify.min.js"></script>
    <link rel="stylesheet" href="alert/css/alertify.min.css" />
    <link rel="stylesheet" href="alert/css/themes/default.min.css" />

</head>

<!-- page wrapper -->
<body class="boxed_wrapper">


<!-- preloader -->
<div class="preloader"></div>
<!-- preloader -->


<!-- search-box-layout -->
<div class="wraper_flyout_search">
    <div class="table">
        <div class="table-cell">
            <div class="flyout-search-layer"></div>
            <div class="flyout-search-layer"></div>
            <div class="flyout-search-layer"></div>
            <div class="flyout-search-close">
                <span class="flyout-search-close-line"></span>
                <span class="flyout-search-close-line"></span>
            </div>
            <div class="flyout_search">
                <div class="flyout-search-title">
                    <h4>Search</h4>
                </div>
                <div class="flyout-search-bar">
                    <form role="search" method="get" action="#">
                        <div class="form-row">
                            <input type="search" placeholder="Type to search..." value="" name="s" required="">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- search-box-layout end -->


<!-- main header -->
<header class="main-header style-seven style-eight">

    <!-- header-upper -->
    <div class="header-upper">
        <div class="container">
            <div class="inner-container clearfix">
                <div class="left-content pull-left">
                    <figure class="logo-box"><a href="/"><img src="images/newlogo2.png" alt=""></a></figure>
                </div>
                <div class="right-content pull-right">
                    <div class="info-box">
                        <div class="icon-box"><i class="flaticon-telephone"></i></div>
                        <h3><a href="tel:0029191762">+254 721 103 761</a></h3>
                        <div class="text">Call for any Support</div>
                    </div>
                    <div class="info-box">
                        <div class="icon-box"><i class="flaticon-placeholder"></i></div>
                        <h3>8th Floor, TSS Towers</h3>
                        <div class="text">Main Office location</div>
                    </div>
                    <ul class="social-list">
                        <li><a href="#"><i class="fab fa-skype"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- header-upper end -->

    <!-- header-bottom -->
    <div class="header-bottom">
        <div class="container">
            <div class="outer-container">
                <div class="nav-outer clearfix">
                    <div class="menu-area pull-left clearfix">
                        <nav class="main-menu navbar-expand-lg">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li class="current"><a href="/">Home</a>
                                    </li>
                                    <li class="dropdown"><a href="#">Products</a>
                                        <ul>
                                            <li><a href="airway.php">Airway management</a></li>
                                            <li><a href="blood.php">Blood Management</a></li>
                                            <li><a href="Electrocardiography.php">Electrocardiography</a></li>
                                            <li><a href="emmergency_care.php">Emergency Care</a></li>
                                            <li><a href="Homecare.php">Home Care</a></li>
                                            <li><a href="invention.php">Interventional Consumables</a></li>
                                            <li><a href="maternal.php">Maternal & Infant Care</a></li>
                                            <li><a href="Neurology.php">Neurology</a></li>
                                            <li><a href="patient_monitoring.php">Patient Monitoring</a></li>
                                            <li><a href="mri.php">MRI Products</a></li>
                                            <li><a href="Ventilation.php">Ventilation</a></li>
                                            <li><a href="vascular.php">Vascular Access</a></li>
                                            <li><a href="Ultrasound.php">Ultrasound</a></li>
                                            <li><a href="Theatre.php">Theatre</a></li>
                                            <li><a href="surgical.php">Surgical Equipment & Consumables</a></li>
                                            <li><a href="cleaning.php">Sterilization & Cleaning</a></li>
                                            <li><a href="Spirometry.php">Spirometry</a></li>
<!--                                            <li><a href="index-3.html">Sleep Diagnostics</a></li>-->
                                            <li><a href="Radiology.php">Radiology</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="about.php">About Us</a></li>
                                    <li ><a href="partners.php">Our Partners</a></li>
                                    <li ><a href="contact.php">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="info-box pull-right clearfix">
                        <div class="btn-box"><a href="products.php">Products<i class="fas fa-arrow-right"></i></a></div>
                        <div class="search-box">
                            <div class="header-flyout-searchbar">
                                <i class="fa fa-search"></i>
                            </div>
                        </div>
<!--                        <div class="shop-cart"><a href="shop.html"><i class="flaticon-shopping-bag"></i><span>0</span></a></div>-->
                        <div class="nav-box">
                            <div class="nav-btn nav-toggler navSidebar-button clearfix">
                                <span class="icon"></span>
                                <span class="icon"></span>
                                <span class="icon"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- header-bottom end -->
    <!--sticky Header-->
    <div class="sticky-header">
        <div class="container clearfix">
            <figure class="logo-box"><a href="/"><img src="" alt=""></a></figure>
            <div class="menu-area">
                <nav class="main-menu navbar-expand-lg">
                    <div class="navbar-header">
                        <!-- Toggle Button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li class="current"><a href="/">Home</a>
                            </li>
                            <li class="dropdown"><a href="#">Products</a>
                                <ul>
                                    <li><a href="airway.php">Airway management</a></li>
                                    <li><a href="blood.php">Blood Management</a></li>
                                    <li><a href="Electrocardiography.php">Electrocardiography</a></li>
                                    <li><a href="emmergency_care.php">Emergency Care</a></li>
                                    <li><a href="Homecare.php">Home Care</a></li>
                                    <li><a href="invention.php">Interventional Consumables</a></li>
                                    <li><a href="maternal.php">Maternal & Infant Care</a></li>
                                    <li><a href="Neurology.php">Neurology</a></li>
                                    <li><a href="patient_monitoring.php">Patient Monitoring</a></li>
                                    <li><a href="mri.php">MRI Products</a></li>
                                    <li><a href="Ventilation.php">Ventilation</a></li>
                                    <li><a href="vascular.php">Vascular Access</a></li>
                                    <li><a href="Ultrasound.php">Ultrasound</a></li>
                                    <li><a href="Theatre.php">Theatre</a></li>
                                    <li><a href="surgical.php">Surgical Equipment & Consumables</a></li>
                                    <li><a href="cleaning.php">Sterilization & Cleaning</a></li>
                                    <li><a href="Spirometry.php">Spirometry</a></li>
                                    <!--                                            <li><a href="index-3.html">Sleep Diagnostics</a></li>-->
                                    <li><a href="Radiology.php">Radiology</a></li>
                                </ul>
                            </li>
                            <li ><a href="about.php">About Us</a></li>
                            <li ><a href="partners.php">Our Partners</a></li>
                            <li ><a href="contact.php">Contact us</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div><!-- sticky-header end -->
</header>
<!-- main-header end -->





