<?php
require_once('navbar.php')
?>



<!-- about-banner -->
<section class="about-banner" style="background-image: url(images/background/about-banner.jpg);">
    <div class="container">
        <div class="content-box">
            <br>
            <br>
            <br>
            <h1>Vascular Access</h1>
            <div class="text"><br /></div>
        </div>
    </div>
</section>
<!-- about-banner end -->
<!-- blog-single -->
<section class="blog-single sidebar-page-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                <div class="sidebar blog-sidebar">
                    <div class="contact-widget sidebar-widget wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="widget-content">
                            <h4>
                                Central Venous Catheters, CVC Accessories, Dialyser Reprocessing Systems, Dialysis Consumables, Implantable Ports, PICC Lines, I.V. Cannulae, Winged Infusion Sets, Pressure Transducers, Pressure Infusion Bags, Extension & Adapters, Stop Cocks, Flow Regulators, Access Sets
                            </h4>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="blog-single-content">
                    <div class="news-block-four">
                        <div class="inner-box">
                            <figure class="image-box"><img src="images/products/vascular-access.jpg" alt=""></figure>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog-single end -->




<?php
require_once('footer.php')
?>
