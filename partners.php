<?php
require_once('navbar.php')
?>



<!-- page-title -->
<section class="page-title" style="background-image: url(images/background/page-title-5.jpg);">
    <div class="container">
        <div class="content-box">
<!--            <h1>Partners</h1>-->
            <br>
            <br>
            <h3>We have small resources but also big dreams and some<br>good ideas</h3>
        </div>
    </div>
</section>
<!-- page-title end -->
<!-- clients-style-two -->
<!-- testimonial-style-three -->
<section class="testimonial-style-three testimonial-page centred">
    <div class="container">
        <div class="title-box">
            <div class="top-text">Our Partners</div>
            <div class="sec-title">
<!--                <h1>What people say about us</h1>-->
                <p><br />Partners</p>
            </div>
        </div>
        <div class="two-column-carousel owl-carousel owl-theme">
            <div class="testimonial-content">
                <div class="inner-box">
                    <div class="text">"Aeonmed's product range consists of robust anaesthetic trolleys, theatre tables, lights and ventilators."</div>
                    <div class="author-info">
                        <figure class=""><img src="images/partner/aeonmed_small.gif" alt=""></figure>
<!--                        <h5>Tamim Anj</h5>-->
<!--                        <span class="designation">Systems Engineer</span>-->
                    </div>
                </div>
            </div>
            <div class="testimonial-content">
                <div class="inner-box">
                    <div class="text">"Aerogen is the global leader in aerosol drug delivery.."</div>
                    <div class="author-info">
                        <figure class=""><img src="images/partner/aerogen_small.gif" alt=""></figure>
<!--                        <h5>Mahfuz Riad</h5>-->
<!--                        <span class="designation">Engineering and Technology</span>-->
                    </div>
                </div>
            </div>
            <div class="testimonial-content">
                <div class="inner-box">
                    <div class="text">"Ambu manufactures several respiratory and life-support devices and neuro diagnostic consumable solutions, as well as an excellent range of medical training aids."</div>
                    <div class="author-info">
                        <figure class=""><img src="images/partner/ambu_small.gif" alt=""></figure>
                        <!--                        <h5>Mahfuz Riad</h5>-->
                        <!--                        <span class="designation">Engineering and Technology</span>-->
                    </div>
                </div>
            </div>
            <div class="testimonial-content">
                <div class="inner-box">
                    <div class="text">"AngioDynamics is a leading provider of innovative medical devices for the minimally-invasive treatment of cancer and peripheral vascular disease. ."</div>
                    <div class="author-info">
                        <figure class=""><img src="images/partner/angiodynamics_small.gif" alt=""></figure>
                        <!--                        <h5>Mahfuz Riad</h5>-->
                        <!--                        <span class="designation">Engineering and Technology</span>-->
                    </div>
                </div>
            </div>
            <div class="testimonial-content">
                <div class="inner-box">
                    <div class="text">"Bentec Medical is a medical device company that was incorporated in 1994."</div>
                    <div class="author-info">
                        <figure class=""><img src="images/partner/bentec_small.gif" alt=""></figure>
                        <!--                        <h5>Mahfuz Riad</h5>-->
                        <!--                        <span class="designation">Engineering and Technology</span>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- testimonial-style-three end -->


<?php
require_once('footer.php')
?>
