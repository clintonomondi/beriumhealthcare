<?php
require_once('navbar.php')
?>



<!-- about-banner -->
<section class="about-banner" style="background-image: url(images/background/about-banner.jpg);">
    <div class="container">
        <div class="content-box">
            <br>
            <br>
            <br>
            <h1>Maternal & Infant Care</h1>
            <div class="text"><br /></div>
        </div>
    </div>
</section>
<!-- about-banner end -->
<!-- blog-single -->
<section class="blog-single sidebar-page-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                <div class="sidebar blog-sidebar">
                    <div class="contact-widget sidebar-widget wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="widget-content">
                            <h4>
                                Incubators, Hybrid Dual Incubator & Infant Warmer, Infant Warmers, Phototherapy, Bubble CPAP, Bassinets, aEEG, Apnea Monitors, Cardiotocographs, Infant Resuscitators, Delivery Beds
                            </h4>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="blog-single-content">
                    <div class="news-block-four">
                        <div class="inner-box">
                            <figure class="image-box"><img src="images/products/division-bannerset5.jpg" alt=""></figure>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog-single end -->




<?php
require_once('footer.php')
?>
