<?php
require_once ('navbar.php')
?>



<!-- about-banner -->
<section class="about-banner" style="background-image: url(images/background/about-banner.jpg);">
    <div class="container">
        <div class="content-box">
            <br>
            <br>
            <h1>Berium Healthcare</h1>
            <div class="text">Our vision for the  next 10 years, to be the most dependable <br /> medical equipment and supplies company in africa</div>
        </div>
    </div>
</section>
<!-- about-banner end -->


<!-- about-style-five -->
<section class="about-style-five">
    <div class="container">
        <div class="inner-container">
            <div class="title-box">
                <div class="top-text">About Berium Healthcare</div>
                <h1>Bringing transformational change to a stagnant industry</h1>
                <h3>Berium Healthcare is an international supplier of a widely diversified portfolio of medical equipment, instruments, tools, devices, accessories and consumables. Berium Healthcare is focused on becoming the market leader in sales and distribution, installation and maintenance of medical infrastructure in Africa. The company is owned by Berium International Ltd and its part of Berium Group Ltd; a global commodity trading company headquartered in the Republic of Cyprus.  .</h3>
            </div>
            <div class="upper-content">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12 image-column">
                        <figure class="image-box"><img src="images/pic4.jpg" alt=""></figure>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 content-column">
                        <div class="content-box wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="icon-box"><i class="flaticon-script"></i></div>
                            <h3 class="group-title">Our Mission</h3>
                            <p>
                                The Mission of Berium Healthcare is to provide exceptional service to the medical industry in Africa. To ensure that the medical equipment and products supplied by Berium Healthcare meet the highest international standards. To seek the highest levels of diligence by investing in progressive staff training and skills development.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower-content">
                <div class="row">
                    <div class="col-lg-5 col-md-12 col-sm-12 left-column">
                        <div class="content-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="icon-box"><i class="flaticon-action"></i></div>
                            <h3 class="group-title">Vision</h3>
                            <p> Our Vision To become a leading company in provision of medical equipment and supplies on the African continent </p>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12 col-sm-12 right-column">
                        <div class="content-box">
                            <h3 class="group-title">Core Valu</h3>
                            <div class="text">
                                <p>      Berium Healthcare’s core philosophy is outstanding client service and an association with only high quality products. We deliver great customer experiences and high quality products. We are proactive in our customer dealings and work hard to retain customers. We make it easy for our customers to do business with us.</p>
                           <p>The values and principles espoused at Berium Healthcare are flexibility, honesty, and value for money. We are dedicated. Our commitment is to make Africa healthy by supporting the medical practitioners, hospitals and health facilities with the hihest quality of medical equipment and supplies.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about-style-five end -->


<!-- counter-style-two -->
<section class="counter-style-two">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="top-text">More</div>
                    <h1>Why Choose Berium?</h1>
                    <div class="text">
                        <p>Berium Healthcare works in a most transparent way. Because we maintain our independence from our manufacturers, we are able to maintain the exacting objectivity required for us to meet only the best interests of our customers. We do not promise the moon if we cannot deliver it. As a result, we have guaranteed level of customer satisfaction.</p>
                        <p>In fact, we try our best to provide our customers the services which they cannot get anywhere else. Our customers very often forget how fast we did a job, but they never forget how well it was done. Most of our customers, with us, believe that Berium Healthcare IS THE BEST, ALL ARE NEXT</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 inner-column">
                <div class="inner-box">
                    <div class="bold-text">We build well-designed, tech-ready multifamily homes 40% faster and 20% less expensive</div>
                    <div class="counter-outer">
                        <div class="counter-block-one wow zoomIn" data-wow-delay="500ms" data-wow-duration="1500ms">
                            <div class="text">Factory Result</div>
                            <div class="count-outer count-box">
                                <span class="count-text" data-speed="1500" data-stop="20">0</span><span>k</span>
                            </div>
                        </div>
                        <div class="counter-block-one wow zoomIn" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="text">Work history</div>
                            <div class="count-outer count-box">
                                <span class="count-text" data-speed="1500" data-stop="10">0</span><span>k</span><span class="symble">+</span>
                            </div>
                        </div>
                        <div class="counter-block-one wow zoomIn" data-wow-delay="500ms" data-wow-duration="1500ms">
                            <div class="text">ward-winning</div>
                            <div class="count-outer count-box">
                                <span class="count-text" data-speed="1500" data-stop="99">0</span><span class="symble">%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- counter-style-two end -->


<!-- statistics-section -->
<section class="statistics-section" style="background-image: url(images/background/intro-bg-3.jpg);">
    <div class="container">
        <div class="outer-container">
            <div class="row">
                <div class="col-xl-7 col-lg-12 col-md-12 offset-xl-5 inner-column">
                    <div class="inner-box wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="video-box"><a href="https://www.youtube.com/watch?v=nfP5N9Yc72A&amp;t=28s" class="lightbox-image" data-caption=""><i class="fas fa-play"></i></a></div>
                        <div class="top-box">
                            <h2>Extensive range</h2>
                            <div class="text">Our extensive range of high-value products also includes Surgical Products, Nursing Consumables, Anesthesia and Respiratory Products, Incontinence & Urology Products, Sterilization Products (CSSD Products), Infection Control Products, Orthopedic Products, Emergency & First Aid Products, Laboratory Consumables, Wound Care Products, Radiology Products, Dental Consumables, IV Solutions, Linen, Uniforms & Shoes, Medical Furniture, Medical Equipment, Surgical & Dental Instruments..</div>

                        </div>
                        <div class="lower-box">
                            <h2>We look forward to partnering with you.</h2>
<!--                            <div class="text">Analyze overall plant capacity by planning for incoming supply and market demand</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- statistics-section end -->


<!-- team-style-two -->

<!-- subscribe-section -->
<section class="subscribe-section about-page bg-color-3">
    <div class="container">
        <div class="inner-box">
            <figure class="icon-box"><img src="images/icons/paper-plane.png" alt=""></figure>
            <div class="content-box">
                <h2>Get Subscribed Now!</h2>
                <div class="text">The latest agopa news, articles, and resources, sent straight to your<br />inbox every month.</div>
                <form action="#" method="post" class="subscribe-form">
                    <div class="form-group">
                        <input type="email" name="email" placeholder="Type Your Email" required="">
                        <button type="submit">Subscribe Now</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- subscribe-section end -->



<?php
require_once ('footer.php')
?>
