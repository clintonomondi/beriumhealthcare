<?php
require_once('navbar.php')
?>



<!-- about-banner -->
<section class="about-banner" style="background-image: url(images/background/about-banner.jpg);">
    <div class="container">
        <div class="content-box">
            <br>
            <br>
            <br>
            <h1>Patient Monitoring</h1>
            <div class="text"><br /></div>
        </div>
    </div>
</section>
<!-- about-banner end -->
<!-- blog-single -->
<section class="blog-single sidebar-page-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                <div class="sidebar blog-sidebar">
                    <div class="contact-widget sidebar-widget wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="widget-content">
                            <h4>
                                SVM Monitor Range, Vismo Monitor Range, Venus Monitor Range, Central Monitors, Triton Monitor Range, Genesis Monitor Range, Transport Monitor Range, CO2 Monitors, Telemetry Central Stations, Telemetry Transmitters, Remote Networking, Pulse Oximeters, Capnographs
                            </h4>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                <div class="blog-single-content">
                    <div class="news-block-four">
                        <div class="inner-box">
                            <figure class="image-box"><img src="images/products/division-bannerset7.jpg" alt=""></figure>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog-single end -->




<?php
require_once('footer.php')
?>
