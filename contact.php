
<?php
require_once('navbar.php')
?>




<!-- contact-banner -->
<section class="contact-banner centred" style="background-image: url(images/background/page-title-6.jpg);">
    <div class="container">
        <div class="content-box">
            <br>
            <br>
            <br>
            <h1>Contact Us</h1>
            <h3>If you have any general enquiries, we'd love to hear from you</h3>
        </div>
    </div>
</section>
<!-- contact-banner end -->


<!-- contact-info-section -->
<section class="contact-info-section contact-page-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12 info-column">
                <div class="single-info-box">
                    <div class="icon-box"><i class="flaticon-location"></i></div>
                    <h3>Location</h3>
                    <div class="text">8th Floor, TSS Towers, Nkrumah Road, P.O Box 82070 – 80100, Mombasa, Kenya.</div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 info-column">
                <div class="single-info-box">
                    <div class="icon-box"><i class="flaticon-telephone"></i></div>
                    <h3>Call us on</h3>
                    <div class="text">
                        <a href="tel:8801912704287">+254 733 185 057</a><br />
                        <a href="tel:029292162">+254 721 103 761</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 info-column">
                <div class="single-info-box">
                    <div class="icon-box"><i class="flaticon-envelope"></i></div>
                    <h3>Email</h3>
                    <div class="text">
                        <a href="mailto:info@example.com">medical@beriumhealthcare.com </a>
                        <a href="mailto:support@example.com">info@beriumhealthcare.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- contact-info-section end -->


<!-- contact-section -->
<section class="contact-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-12 col-sm-12 form-column">
                <div class="contact-form-area">
                    <h2>Drop us a line</h2>
                    <form method="post"   class="default-form" role="form" id="submitKasae" onsubmit="return false">
                        <div class="form-group">
                            <input type="text" name="name" placeholder="Your Name*" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" placeholder="Your Email*" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="subject" placeholder="Subject" required>
                        </div>
                        <div class="form-group">
                            <textarea name="message" placeholder="Your Message"></textarea>
                        </div>
                        <div class="form-group message-btn">
                            <button type="submit" id="submit" class="theme-btn" name="submit-form">Submit a Message</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 map-column">

                    <div class="container">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7567699590722!2d36.901303914685805!3d-1.3217246990359888!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f12f62d8476eb%3A0x3a52af9210404c54!2sAirport%20N%20Rd%2C%20Nairobi!5e0!3m2!1sen!2ske!4v1584185550512!5m2!1sen!2ske" width="1200" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- contact-section end -->


<?php
require_once('footer.php')
?>
