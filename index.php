

<?php
require_once ('navbar.php')
?>


    <!-- main-slider -->
    <section class="slider-style-seven home-8">
        <div class="main-slider-carousel-2 owl-carousel owl-theme">
            <div class="slide" style="background-image:url(images/main-slider/pic3.jpg)">
                <div class="container">
                    <div class="content-box">
                        <h5>Medical</h5>
                        <h1>Berium Healthcare<br />  Best medical supplier</h1>
                        <div class="text">Fastest Growing medical equipment supply company<br />that create a modern and safe</div>
                        <div class="btn-box"><a href="#">explore products</a></div>
                    </div>
                </div>
            </div>
            <div class="slide" style="background-image:url(images/main-slider/slider-8.jpg)">
                <div class="container">
                    <div class="content-box">
                        <h5>Medical</h5>
                        <h1>Berium Health<br />  Best medical supplier</h1>
                        <div class="text">Satisfying our clients and building affordable<br /> helthcare systems.</div>
                        <div class="btn-box"><a href="#">explore products</a></div>
                    </div>
                </div>
            </div>
            <div class="slide" style="background-image:url(images/main-slider/pic3.jpg)">
                <div class="container">
                    <div class="content-box">
                        <h5>Medical</h5>
                        <h1>Berium Healthcare<br /> Best medical Equipment Supplier</h1>
                        <div class="text">With long standing experience,<br />we distinguish ourselves by maintaining high standard in all product and process</div>
                        <div class="btn-box"><a href="#">explore products</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- main-slider end -->


    <!-- fabrics-section -->
    <section class="fabrics-section home-8">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 carousel-column">
                    <div class="single-item-carousel owl-carousel owl-theme">
                        <figure class="slide-item"><img src="images/resource/medical-1.jpg" alt=""></figure>
                        <figure class="slide-item"><img src="images/resource/medical-1.jpg" alt=""></figure>
                        <figure class="slide-item"><img src="images/resource/medical-1.jpg" alt=""></figure>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                    <div class="content-box">
                        <div class="top-text">Small products, big future.</div>
                        <h1>We Provide You With Access To The Best  Medical  Supplies and  equipment</h1>
                        <div class="bold-text">We have  experience in transforming brilliant ideas into commercially successful outcomes</div>
                        <div class="text">Whether it be thousands of units or millions, the true power and profitability of a product lies in its ability to reach out en mass and change people's lives.</div>
<!--                        <div class="btn-box"><a href="#">Read more</a></div>-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- fabrics-section end -->


    <!-- intro-style-five -->
    <section class="intro-style-five centred">
        <div class="container">
            <div class="title-box">
                <h2>We provide innovative medical equipment and supplies.</h2>
                <p>Berium Healthcare provides innovative equipment designed and manufactured  visionary proffessonal and experience global companies .</p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                    <div class="single-item wow fadeInUp animated animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="icon-box"><i class="flaticon-serum"></i></div>
                        <h3><a href="#">Invasive treatments</a></h3>
                        <div class="text">We partner with reputable providers of textile testing needs to assure compliance with changing governmental safety</div>
                        <div class="btn-box"><a href="#">Explore</a></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                    <div class="single-item wow fadeInUp animated animated" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="icon-box"><i class="flaticon-foetal"></i></div>
                        <h3><a href="#">Microfluidic technologies</a></h3>
                        <div class="text">This is where the magic happens. Our state of the art facilities and unique mix of technologies</div>
                        <div class="btn-box"><a href="#">Explore</a></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                    <div class="single-item wow fadeInUp animated animated" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="icon-box"><i class="flaticon-dropper"></i></div>
                        <h3><a href="#">Bioscience</a></h3>
                        <div class="text">We integrate bioscience expertise and infrastructure into our projects teams.</div>
                        <div class="btn-box"><a href="#">Explore</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- intro-style-five end -->


    <!-- intro-style-six -->
    <section class="intro-style-six" style="background-image: url(images/background/intro-bg-3.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                    <div class="content-box">
                        <div class="top-box">
                            <h2>Berium Healthcare provides reliable supply and equipment</h2>
                            <div class="text">
                                <p>Designed and manufactured in Finland, the award-winning Q-Flow™ is the light of choice for every surgical team</p>
                                <p>Merivaara Q-Flow™ is the intelligent solution for any operating theatre. The design is optimised for the OT’s airflow, improving airflow circulation in the operating area and reducing the potential for contamination.</p>
                            </div>
                            <h3>Create a modern and safe environment for your patients</h3>
                        </div>
                        <div class="lower-box wow fadeInLeft animated animated" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <div class="video-box"><a href="https://www.youtube.com/watch?v=nfP5N9Yc72A&amp;t=28s" class="lightbox-image" data-caption=""><i class="flaticon-play-button"></i></a></div>
                            <div class="inner-content">
                                <h3>Africa Wide Medical Equipment Supplier</h3>
                                <p>All of your  Medical Equipment Supplier needs on one place. Your one stop store offering new, refurbished, and used \Medical Equipment .CT Scanner, MRI, Ultrasound, X Ray, Mammography, Nuclear medicine, PET, PET/CT, Special Procedure Suites</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                    <figure class="image-box wow fadeInRight animated animated" data-wow-delay="00ms" data-wow-duration="1500ms"><img src="images/resource/medical-2.jpg" alt=""></figure>
                </div>
            </div>
        </div>
    </section>
    <!-- intro-style-six end -->


    <!-- product-section -->
    <section class="product-section centred">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 product-block">
                    <div class="product-block-one wow fadeInUp animated animated" data-wow-delay="00ms">
                        <div class="inner-box">
                            <figure class="image-box"><img src="images/resource/shop/product-1.jpg" alt=""></figure>
                            <div class="content-box"><a href="#">Diagnostic Products</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 product-block">
                    <div class="product-block-one wow fadeInUp animated animated" data-wow-delay="300ms">
                        <div class="inner-box">
                            <figure class="image-box"><img src="images/resource/shop/product-2.jpg" alt=""></figure>
                            <div class="content-box"><a href="#">Wheelchairs Products</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 product-block">
                    <div class="product-block-one wow fadeInUp animated animated" data-wow-delay="600ms">
                        <div class="inner-box">
                            <figure class="image-box"><img src="images/resource/shop/product-3.jpg" alt=""></figure>
                            <div class="content-box"><a href="#">Ohaus Products</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- product-section end -->


    <!-- medical-shop -->
    <section class="medical-shop">
        <div class="container">
            <div class="title-box centred">
                <div class="top-text">Medical Product</div>
                <h1>Medical equipment</h1>
                <p>We are continually adding new products, expanding our referral sources, and increasing<br />profits so that we can continue the growth</p>
            </div>
        </div>
        <div class="container-fluid">
            <div class="inner-content">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <figure class="image-box"><img src="images/resource/shop/medical-porduct-1.jpg" alt=""></figure>
                                <div class="content-box">
                                    <h4><a href="#">Nuclear medicine</a></h4>
                                    <div class="text">12 Product</div>
                                </div>
                                <div class="overlay-box">
                                    <div class="box">
                                        <div class="view-btn"><a href="images/resource/shop/medical-porduct-1.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-add"></i></a></div>
                                        <h4><a href="#">Nuclear medicine</a></h4>
                                        <div class="text">15 Product</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <figure class="image-box"><img src="images/resource/shop/medical-porduct-2.jpg" alt=""></figure>
                                <div class="content-box">
                                    <h4><a href="#">Ultrasound, X Ray</a></h4>
                                    <div class="text">15 Product</div>
                                </div>
                                <div class="overlay-box">
                                    <div class="box">
                                        <div class="view-btn"><a href="images/resource/shop/medical-porduct-2.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-add"></i></a></div>
                                        <h4><a href="#">Ultrasound, X Ray</a></h4>
                                        <div class="text">15 Product</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                        <div class="single-item">
                            <div class="inner-box">
                                <figure class="image-box"><img src="images/resource/shop/medical-porduct-3.jpg" alt=""></figure>
                                <div class="content-box">
                                    <h4><a href="#">PORTABLES X-RAY/C-ARM</a></h4>
                                    <div class="text">20 Product</div>
                                </div>
                                <div class="overlay-box">
                                    <div class="box">
                                        <div class="view-btn"><a href="images/resource/shop/medical-porduct-3.jpg" class="lightbox-image" data-fancybox="gallery"><i class="flaticon-add"></i></a></div>
                                        <h4><a href="#">PORTABLES X-RAY/C-ARM</a></h4>
                                        <div class="text">20 Product</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="lower-content wow fadeInUp animated animated" data-wow-delay="00ms">
                <h3>We are committed to providing customers with superior equipment that meet their medical needs, while maintaining quality</h3>
                <p>We are continually adding new products, expanding our referral sources, and increasing profits so that we can continue the growth we have experienced.</p>
                <a href="products.php">View Products</a>
            </div>
        </div>
    </section>
    <!-- medical-shop end -->


    <!-- testimonial-style-seven -->
    <section class="testimonial-style-seven centred">
        <div class="container">
            <div class="sec-title">
                <div class="top-text">Client</div>
                <h1>See what our customers say</h1>
            </div>
            <div class="single-item-carousel owl-carousel owl-theme">
                <div class="testimonial-content">
                    <div class="inner-box">
                        <div class="text">"Using medical products supplied by Berium,has helped me satisfy my customers by and large in every way. The availability of all kinds of medical products required under one umbrella has helped us save on alot of effort and time. Their keen desire to grow and satisfy their customers has propelled me to come back gain."</div>
                        <div class="author-info">
                            <figure class="author-thumb"><img src="images/resource/testimonial-6.png" alt=""></figure>
                            <h5>Tamim Anj</h5>
                            <span class="designation">Doc Tamim Anj  FCA</span>
                        </div>
                    </div>
                </div>
                <div class="testimonial-content">
                    <div class="inner-box">
                        <div class="text">"Using medical products supplied by Berium,has helped me satisfy my customers by and large in every way. The availability of all kinds of medical products required under one umbrella has helped us save on alot of effort and time. Their keen desire to grow and satisfy their customers has propelled me to come back gain."</div>
                        <div class="author-info">
                            <figure class="author-thumb"><img src="images/resource/testimonial-6.png" alt=""></figure>
                            <h5>Tamim Anj</h5>
                            <span class="designation">Doc Tamim Anj  FCA</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial-style-seven end -->




    <!-- signup-section -->
    <section class="signup-section">
        <div class="container">
            <div class="inner-container">
                <div class="inner-box clearfix">
<!--                    <div class="text pull-left"><h5>Berium is a Worldwide supply of Medical Equipment Since 1991</h5></div>-->
                    <div class="btn-box pull-right"><a href="products.php">Products</a></div>
                </div>
            </div>
        </div>
    </section>
    <!-- signup-section end -->


<?php
require_once ('footer.php')
?>
