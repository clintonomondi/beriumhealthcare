


<!-- main-footer -->
<footer class="main-footer style-five home-8">
    <div class="container">
        <div class="footer-top">
            <div class="widget-section clearfix">
                <div class="contact-widget footer-widget">
                    <h4 class="widget-title">Information</h4>
                    <div class="widget-content">
                        <div class="text">8th Floor, TSS Towers, Nkrumah Road, P.O Box 82070 – 80100, Mombasa, Kenya.</div>
                        <ul class="info-list">
                            <li>Email: <a href="mailto:info@example.com">medical@beriumhealthcare.com info@beriumhealthcare.com </a></li>
                            <li>Phone: <a href="tel:Phone:+254 733 185 057">+254 733 185 057</a></li>
                        </ul>
                    </div>
                </div>
                <div class="links-widget company-widget footer-widget">
                    <h4 class="widget-title">Company</h4>
                    <div class="widget-content">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Products</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="links-widget footer-widget">
                    <h4 class="widget-title">Products</h4>
                    <div class="widget-content">
                        <ul>
                            <li><a href="#">Medical Disposables</a></li>
                            <li><a href="#">Hospital and medical furniture</a></li>
                            <li><a href="#">Anaesthesia products & Equipments</a></li>
                            <li><a href="#">Sterialization equipments & accessories</a></li>
                            <li><a href="#">Strobel</a></li>
                        </ul>
                    </div>
                </div>
                <div class="links-widget footer-widget">
                    <h4 class="widget-title">Countries</h4>
                    <div class="widget-content">
                        <ul>
                            <li><a href="#">Explore countries</a></li>
                            <li><a href="#">Members</a></li>
                            <li><a href="#">Association</a></li>
                            <li><a href="#">Jobs</a></li>
                            <li><a href="#">Training</a></li>
                        </ul>
                    </div>
                </div>
                <div class="social-widget footer-widget">
                    <h4 class="widget-title">Get the latest from</h4>
                    <ul class="social-links">
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-slack-hash"></i></a></li>
                        <li><a href="#"><i class="fab fa-dribbble"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom clearfix">
            <figure class="footer-logo pull-left"><a href="/"><img src="" alt=""></a></figure>
            <div class="copyright pull-right">Copyright 2020, All Right Reserved</div>
        </div>
    </div>
</footer>
<!-- main-footer end -->



<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-arrow-up"></span>
</button>


<!-- sidebar cart item -->
<div class="xs-sidebar-group info-group info-sidebar">
    <div class="xs-overlay xs-bg-black"></div>
    <div class="xs-sidebar-widget">
        <div class="sidebar-widget-container">
            <div class="widget-heading">
                <a href="#" class="close-side-widget">
                    X
                </a>
            </div>
            <div class="sidebar-textwidget">

                <!-- Sidebar Info Content -->
                <div class="sidebar-info-contents">
                    <div class="content-inner">
                        <div class="logo">
                            <a href="/"><img src="" alt="" /></a>
                        </div>
                        <div class="content-box">
                            <h4>About Us</h4>
                            <p class="text">Berium Healthcare is an international supplier of a widely diversified portfolio of medical equipment, instruments, tools, devices, accessories and consumables. Berium Healthcare is focused on becoming the market leader in sales and distribution, installation and maintenance of medical infrastructure in Africa. The company is owned by Berium International Ltd and its part of Berium Group Ltd; a global commodity trading company headquartered in the Republic of Cyprus. .</p>
                            <a href="about.php">Explore</a>
                        </div>
                        <div class="contact-info">
                            <h4>Contact Info</h4>
                            <ul>
                                <li>8th Floor, TSS Towers, Nkrumah Road, P.O Box 82070 – 80100, Mombasa, Kenya.</li>
                                <li><a href="tel:+8801682648101"> +254 733 185 057
                                        +254 721 103 761 </a></li>
                                <li><a href="mailto:info@example.com"> medical@beriumhealthcare.com, info@beriumhealthcare.com </a></li>
                            </ul>
                        </div>
                        <!-- Social Box -->
                        <ul class="social-box">
                            <li class="facebook"><a href="#" class="fab fa-facebook-f"></a></li>
                            <li class="twitter"><a href="#" class="fab fa-twitter"></a></li>
                            <li class="linkedin"><a href="#" class="fab fa-linkedin-in"></a></li>
                            <li class="instagram"><a href="#" class="fab fa-instagram"></a></li>
                            <li class="youtube"><a href="#" class="fab fa-youtube"></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END sidebar widget item -->



<!-- jequery plugins -->
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/validation.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/appear.js"></script>
<script src="js/aos.js"></script>
<script src="js/isotope.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/nav-tool.js"></script>

<!-- main-js -->
<script src="js/script.js"></script>
<script src="js/api.js"></script>

</body><!-- End of .page_wrapper -->

<!-- Mirrored from azim.commonsupport.com/Acto/index-8.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Apr 2020 10:11:25 GMT -->
</html>
