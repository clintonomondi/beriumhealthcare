<?php
require_once('navbar.php')
?>



<!-- service-banner -->
<section class="service-banner" style="background-image: url(images/main-slider/pic3.jpg);">
    <div class="container">
        <div class="content-box">
<!--            <h1>Industries Solution</h1>-->
            <br>
            <br>
            <br>
            <br>
            <h3><br />PRODUCTS</h3>
        </div>
    </div>
</section>
<!-- service-banner end -->

<!-- service-section -->
<section class="service-section service-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 service-block">
                <div class="service-block-one wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="Radiology.php"><img src="images/resource/pic3.jpg" alt=""></a></figure>
                        <div class="content-box">
                            <div class="icon-box"><i class="flaticon-factory"></i></div>
                            <h3><a href="Radiology.php">Radiology</a></h3>
                            <div class="text"> Digital Radiography Systems, Analogue Radiography Systems, C-Arms, Fluoroscopy Tables, Mammography, Specimen Radiography Systems, MRI Products</div>
                            <div class="link-btn"><a href="Radiology.php">Go Continues<i class="fas fa-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 service-block">
                <div class="service-block-one wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="Neurology.php"><img src="images/resource/pic3.jpg" alt=""></a></figure>
                        <div class="content-box">
                            <div class="icon-box"><i class="flaticon-oil"></i></div>
                            <h3><a href="Neurology.php">NeuRology</a></h3>
                            <div class="text">EEG, IOM (Intraoperative Monitoring), EMG, Ambulatory EEG </div>
                            <div class="link-btn"><a href="Neurology.php">Go Continues<i class="fas fa-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 service-block">
                <div class="service-block-one wow fadeInLeft" data-wow-delay="500ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="Homecare.php"><img src="images/resource/pic3.jpg" alt=""></a></figure>
                        <div class="content-box">
                            <div class="icon-box"><i class="flaticon-nuclear-plant"></i></div>
                            <h3><a href="Homecare.php">Home Care</a></h3>
                            <div class="text">CPAP/BiPAP®–Sleep Devices, BiPAP®–Non Invasive Ventilators (NPPV), Ventilators/BiPAP®–Non Invasive Ventilators & Invasive (Life Support), Bronchial Hygiene, Oxygen Concentrators, Pulse Oximeters, Asthma Management, Nebulisers, Masks </div>
                            <div class="link-btn"><a href="Homecare.php">Go Continues<i class="fas fa-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 service-block">
                <div class="service-block-one wow fadeInRight" data-wow-delay="500ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="Ventilation.php"><img src="images/resource/pic3.jpg" alt=""></a></figure>
                        <div class="content-box">
                            <div class="icon-box"><i class="flaticon-factory-machine"></i></div>
                            <h3><a href="Ventilation.php">Ventilations</a></h3>
                            <div class="text">Ventilators, Infant Ventilators, nCPAP Drivers, Hospital Humidification, Cuff Pressure Controllers, Hospital Masks, Air/ Oxygen Blenders </div>
                            <div class="link-btn"><a href="Ventilation.php">Go Continues<i class="fas fa-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 service-block">
                <div class="service-block-one wow fadeInLeft" data-wow-delay="1000ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="Ultrasound.php"><img src="images/resource/pic3.jpg" alt=""></a></figure>
                        <div class="content-box">
                            <div class="icon-box"><i class="flaticon-wind-energy"></i></div>
                            <h3><a href="Ultrasound.php">Ultra Sound</a></h3>
                            <div class="text">Cart-Based Ultrasound, Handheld Ultrasound, Tablet Devices. </div>
                            <div class="link-btn"><a href="Ultrasound.php">Go Continues<i class="fas fa-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 service-block">
                <div class="service-block-one wow fadeInRight" data-wow-delay="1000ms" data-wow-duration="1500ms">
                    <div class="inner-box">
                        <figure class="image-box"><a href="emmergency_care.php"><img src="images/resource/pic3.jpg" alt=""></a></figure>
                        <div class="content-box">
                            <div class="icon-box"><i class="flaticon-support"></i></div>
                            <h3><a href="emmergency_care.php">Emergency Care</a></h3>
                            <div class="text">Resuscitators, Automated External Defibrillators, Training Manikins, Oesphageal Intubation Detector, Manometer, Face Masks, Emergency Ventilators, Suction </div>
                            <div class="link-btn"><a href="emmergency_care.php">Go Continues<i class="fas fa-arrow-right"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- service-section end -->





<?php
require_once('footer.php')
?>
